module gitlab.com/CoiaPrant/tls

go 1.18

require (
	github.com/andybalholm/brotli v1.0.4
	github.com/klauspost/compress v1.15.11
	github.com/refraction-networking/utls v1.1.5
	golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90
	golang.org/x/sys v0.0.0-20220829200755-d48e67d00261
)
